/*
 * Copyright (c) BetForTrade, All Rights Reserved.
 * (www.betfortrade.com)
 *
 * This software is the proprietary information of BetForTrade.
 * Use is subject to license terms.
 *
 */
package com.mcmartins.reuse.core.resource.api;

import java.util.Locale;

/**
 * Handles resource bundles based on a request class and a locale.
 *
 * @author Manuel Martins
 */
public interface ResourceBundleHandler<T> {

    /**
     * Returns the resource resolver for a specified class and locale.
     *
     * @param clazz the class which requests the resource.
     * @param locale the locale for the resource translation.
     * @return the resource resolver.
     */
    ResourceResolver<T> getResourceResolver(Class<?> clazz, Locale locale);

}
