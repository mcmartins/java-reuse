/*
 * Copyright (c) BetForTrade, All Rights Reserved.
 * (www.betfortrade.com)
 *
 * This software is the proprietary information of BetForTrade.
 * Use is subject to license terms.
 *
 */
package com.mcmartins.reuse.core.exception.api;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.MessageFormat;
import java.util.UUID;

import com.google.common.hash.HashCode;
import com.google.common.hash.HashFunction;
import com.google.common.hash.Hashing;
import com.mcmartins.reuse.core.common.Keyable;

/**
 * <Description>.
 *
 * @author Manuel Martins
 */
final class ExceptionKey implements Keyable<Integer> {

    private final int key;
    private String machineName;

    /**
     * Default constructor.
     */
    ExceptionKey() {
        final HashFunction hashFunction = Hashing.md5();
        final HashCode hashCode = hashFunction.newHasher().putString(UUID.randomUUID().toString()).hash();
        this.key = hashCode.asInt();
        this.machineName = "UNKNOWN";
        try {
            InetAddress localMachine = java.net.InetAddress.getLocalHost();
            if (localMachine != null) {
                this.machineName = localMachine.getHostName();
            }
        } catch (final UnknownHostException ignore) {
        }
    }

    @Override
    public Integer getKey() {
        return this.key;
    }

    @Override
    public String toString() {
        return MessageFormat.format("[{0}:{1}]", this.machineName, String.valueOf(this.key));
    }
}
