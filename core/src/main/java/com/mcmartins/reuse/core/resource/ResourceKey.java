/*
 * Copyright (c) BetForTrade, All Rights Reserved.
 * (www.betfortrade.com)
 *
 * This software is the proprietary information of BetForTrade.
 * Use is subject to license terms.
 *
 */
package com.mcmartins.reuse.core.resource;

import java.text.MessageFormat;
import java.util.Locale;

import com.google.common.base.Preconditions;
import com.mcmartins.reuse.core.common.Keyable;

/**
 * <Description>.
 *
 * @author Manuel Martins
 */
final class ResourceKey implements Keyable<String> {

    private final String packageName;
    private final Locale locale;

    /**
     * Default constructor.
     *
     * @param packageName the package.
     * @param locale the locale.
     */
    ResourceKey(final String packageName, final Locale locale) {
        this.packageName = Preconditions.checkNotNull(packageName);
        this.locale = locale == null ? Locale.getDefault() : locale;
    }

    public static Keyable<String> getInstance(final String packageName, final Locale locale) {
        return new ResourceKey(packageName, locale);
    }

    @Override
    public String getKey() {
        return MessageFormat.format("[{0}_{1}]", this.packageName, this.locale);
    }

    @Override
    public boolean equals(final Object obj) {
        if (obj instanceof Keyable) {
            return this.getKey().equals(((Keyable) obj).getKey());
        }
        return super.equals(obj);
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += this.getKey() != null ? this.getKey().hashCode() : 0;
        return hash;
    }

    @Override
    public String toString() {
        return this.getKey();
    }
}
