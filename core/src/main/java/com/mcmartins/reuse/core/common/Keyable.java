/*
 * Copyright (c) BetForTrade, All Rights Reserved.
 * (www.betfortrade.com)
 *
 * This software is the proprietary information of BetForTrade.
 * Use is subject to license terms.
 *
 */
package com.mcmartins.reuse.core.common;

/**
 * <Description>.
 *
 * @author Manuel Martins
 */
public interface Keyable<T> {

    /**
     * Gets the unique identifier key.
     *
     * @return key an unique id.
     */
    T getKey();
}
