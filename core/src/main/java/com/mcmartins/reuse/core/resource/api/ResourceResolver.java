/*
 *  Copyright 2013 Manuel Martins.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  under the License.
 */
package com.mcmartins.reuse.core.resource.api;

import com.mcmartins.reuse.core.common.Keyable;

/**
 * Resolves resources by code.
 *
 * @author Manuel Martins
 */
public interface ResourceResolver<T> {

    /**
     * Resolves a resource code.
     *
     * @param resourceCode the resource code.
     * @param parameters the parameters to add to the message, if any.
     * @return the resource translated.
     */
    String resolve(Keyable<T> resourceCode, Object... parameters);

}
