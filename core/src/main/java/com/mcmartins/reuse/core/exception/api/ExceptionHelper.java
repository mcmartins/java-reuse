/*
 * Copyright (c) BetForTrade, All Rights Reserved.
 * (www.betfortrade.com)
 *
 * This software is the proprietary information of BetForTrade.
 * Use is subject to license terms.
 *
 */
package com.mcmartins.reuse.core.exception.api;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import com.google.common.base.Preconditions;
import com.mcmartins.reuse.core.common.Keyable;
import com.mcmartins.reuse.core.resource.ResourceResolverFactory;

/**
 * This class resolves exceptions resources based on specific resource location.
 *
 * @author Manuel Martins
 */
final class ExceptionHelper {

    private final Keyable<Integer> uniqueIdentifier = new ExceptionKey();
    private final List<Object> parameters = new ArrayList<Object>();
    private final Keyable resourceCode;

    private LoggableInfo loggableInfo;

    /**
     * Default constructor.
     *
     * @param resourceCode the resource code to resolve.
     */
    ExceptionHelper(final Keyable<?> resourceCode) {
        this.resourceCode = Preconditions.checkNotNull(resourceCode);
    }

    /**
     * Creates a localized description of this throwable in a specified locale.
     *
     * @param locale the locale.
     * @param cause the throwable cause.
     * @param withIdentifier {@code true} to print the unique identifier.
     * @return the localized description.
     */
    @SuppressWarnings("unchecked")
    public String getLocalizedMessage(final Class<?> cause, final Locale locale, final boolean withIdentifier) {
        return MessageFormat.format("{0}{1}",
                ResourceResolverFactory.INSTANCE.getResource(cause, locale, this.resourceCode, this.parameters),
                withIdentifier ? this.uniqueIdentifier : "");
    }

    /**
     * Creates a localized description of this throwable in a specified locale.
     *
     * @return the additional message.
     */
    @SuppressWarnings("unchecked")
    public String getMessage() {
        return this.loggableInfo != null ? this.loggableInfo.getFormattedLogMessage() : null;
    }

    /**
     * Sets the additional information for the current exception.
     *
     * @param info the additional information object.
     */
    public void setLoggableInfo(final LoggableInfo info) {
        this.loggableInfo = info;
    }

    /**
     * Get unique identifier for this exception.
     *
     * @return the unique identifier.
     */
    public Keyable<Integer> getUniqueIdentifier() {
        return uniqueIdentifier;
    }

    /**
     * Get parameters.
     *
     * @return parameters.
     */
    public List<Object> getParameters() {
        return parameters;
    }

    /**
     * Get resource code.
     *
     * @return resourceCode.
     */
    public Keyable<?> getResourceCode() {
        return resourceCode;
    }
}
