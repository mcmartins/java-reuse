/*
 *  Copyright 2013 Manuel Martins.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  under the License.
 */
package com.mcmartins.reuse.core.resource;

import java.util.Locale;

import com.mcmartins.reuse.core.common.Keyable;
import com.mcmartins.reuse.core.resource.api.ResourceBundleHandler;

/**
 * <Description>.
 *
 * @author Manuel Martins
 */
public enum ResourceResolverFactory {

    INSTANCE(new ResourceBundleResolver());

    private ResourceBundleHandler<String> resourceBundleHandler;

    /**
     * Default constructor.
     *
     * @param resourceBundleHandler the resource handler.
     */
    ResourceResolverFactory(final ResourceBundleHandler<String> resourceBundleHandler) {
        this.resourceBundleHandler = resourceBundleHandler;
    }

    /**
     * Returns the resource string for the specified resource code.
     *
     * @param clazz the class which requests the resource.
     * @param locale the locale for the resource translation.
     * @param resourceCode the resource code.
     * @param parameters the parameters for the message if any.
     * @return the resource translated.
     */
    public String getResource(final Class<?> clazz, final Locale locale,
            final Keyable<String> resourceCode, final Object... parameters) {
        return resourceBundleHandler.getResourceResolver(clazz, locale).resolve(resourceCode, parameters);
    }
}
