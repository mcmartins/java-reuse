/*
 * Copyright (c) BetForTrade, All Rights Reserved.
 * (www.betfortrade.com)
 *
 * This software is the proprietary information of BetForTrade.
 * Use is subject to license terms.
 *
 */
package com.mcmartins.reuse.core.exception.api;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Locale;

import com.google.common.base.Objects;
import com.mcmartins.reuse.core.common.Keyable;

/**
 * Holds utility methods to handle Runtime Exceptions.
 *
 * @author Manuel Martins
 */
public abstract class AbstractRuntimeException extends RuntimeException {

    private final ExceptionHelper exceptionHelper;

    /**
     * Default constructor.
     *
     * @param cause the throwable.
     */
    public AbstractRuntimeException(final Throwable cause) {
        super(cause);
        this.exceptionHelper = new ExceptionHelper(new Keyable<String>() {
            @Override
            public String getKey() {
                return "core.exception.no.resource";
            }
        });
    }

    /**
     * Default constructor.
     */
    public AbstractRuntimeException() {
        this.exceptionHelper = new ExceptionHelper(new Keyable<String>() {
            @Override
            public String getKey() {
                return "core.exception.no.resource";
            }
        });
    }

    /**
     * Default constructor.
     *
     * @param resourceCode the resource code to translate.
     * @param cause the throwable.
     */
    public AbstractRuntimeException(final Keyable<?> resourceCode, final Throwable cause) {
        super(cause);
        this.exceptionHelper = new ExceptionHelper(resourceCode);
    }

    @Override
    public String getLocalizedMessage() {
        return this.getLocalizedMessage(null, false);
    }

    @Override
    public String getMessage() {
        return MessageFormat.format("{0} \n {1}", this.exceptionHelper.getMessage(), super.getMessage());
    }

    /**
     * Creates a localized description of this throwable in a specified locale.
     *
     * @param locale the locale.
     * @param withIdentifier {@code true} to print the unique identifier.
     * @return the localized description.
     */
    public String getLocalizedMessage(final Locale locale, final boolean withIdentifier) {
        return this.exceptionHelper.getLocalizedMessage(this.getClass(), locale, withIdentifier);
    }

    /**
     * Add parameters for the localized message.
     *
     * @param parameter the parameter.
     */
    protected void addParameters(final Object... parameter) {
        if (parameter != null && !Objects.firstNonNull(Arrays.asList(parameter), new ArrayList<Object>()).isEmpty()) {
            this.exceptionHelper.getParameters().addAll(Arrays.asList(parameter));
        }
    }

    /**
     * Adds additional information for log purposes. Adds the content of {@link com.java.reuse.core.exception.api.LoggableInfo#getFormattedLogMessage()}
     * before the {@link Exception#getMessage()}.
     *
     * @param info the information to add.l
     */
    protected void setAdditionalInfo(final LoggableInfo info) {
        this.exceptionHelper.setLoggableInfo(info);
    }

    /**
     * Returns the unique identifier for this exception.
     *
     * @return uniqueIdentifier the identifier.
     */
    public Keyable<?> getUniqueIdentifier() {
        return this.exceptionHelper.getUniqueIdentifier();
    }

    /**
     * Returns the resource code.
     *
     * @return code the resource code.
     */
    public Keyable getResourceCode() {
        return this.exceptionHelper.getResourceCode();
    }
}
