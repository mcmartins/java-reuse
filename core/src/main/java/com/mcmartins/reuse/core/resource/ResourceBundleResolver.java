/*
 * Copyright (c) BetForTrade, All Rights Reserved.
 * (www.betfortrade.com)
 *
 * This software is the proprietary information of BetForTrade.
 * Use is subject to license terms.
 *
 */
package com.mcmartins.reuse.core.resource;

import java.text.MessageFormat;
import java.util.Locale;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import com.google.common.base.Preconditions;
import com.mcmartins.reuse.core.common.Keyable;
import com.mcmartins.reuse.core.resource.api.ResourceBundleHandler;
import com.mcmartins.reuse.core.resource.api.ResourceResolver;

/**
 * <Description>.
 *
 * @author Manuel Martins
 */
class ResourceBundleResolver implements ResourceResolver<String>, ResourceBundleHandler<String> {

    private static final Logger LOG = Logger.getLogger(ResourceBundleResolver.class);

    private static final String RESOURCES_PACKAGE = "resources.resource";

    private static final Map<Keyable<String>, ResourceResolver<String>> resourceResolvers =
            new ConcurrentHashMap<Keyable<String>, ResourceResolver<String>>();

    private ResourceBundle resourceBundle;

    /**
     * Default constructor.
     */
    ResourceBundleResolver() {
    }

    @Override
    public String resolve(final Keyable<String> resourceCode, final Object... parameters) {
        String message = Preconditions.checkNotNull(resourceCode).getKey();

        if (this.resourceBundle != null) {
            try {
                message = this.resourceBundle.getString(resourceCode.getKey());
            } catch (final MissingResourceException e) {
                LOG.error(e);
                message = e.getMessage();
            }

            if (message != null) {
                if (parameters != null) {
                    // Replaces parameters
                    message = MessageFormat.format(message, parameters);
                }
            }
        }
        return message;
    }

    @Override
    public synchronized ResourceResolver<String> getResourceResolver(final Class<?> resourceClass, Locale locale) {
        final String packageName = Preconditions.checkNotNull(resourceClass).getPackage().getName();
        final Keyable<String> key = ResourceKey.getInstance(packageName, locale);
        ResourceResolver<String> handler = resourceResolvers.get(key);
        if (handler == null) {
            try {
                locale = locale == null ? Locale.getDefault() : locale;
                // Load resources from file <packageName>.resources.resources[<locale>].properties using resources class loader.
                this.resourceBundle =
                        ResourceBundle.getBundle(MessageFormat.format("{0}.{1}", packageName, RESOURCES_PACKAGE), locale,
                                resourceClass.getClassLoader());
            } catch (final MissingResourceException e) {
                LOG.error(e);
            }
            handler = this;
            resourceResolvers.put(key, handler);
        }
        return handler;
    }
}
